import React from 'react';

// Bootstrap Components
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export default function CourseCard() {
	return (
		<Card style={{ width: '18rem' }}>
  <Card.Body>
    <Card.Title>Sample Course</Card.Title>
    <Card.Subtitle className="mb-2 text-muted">Description</Card.Subtitle>
    <Card.Text>
     This is a sample courrse offering. 
      Price:
      PHP 40,000
    </Card.Text>
    <Card.Link href="#">Enroll</Card.Link>
    
  </Card.Body>
</Card>
		)
}